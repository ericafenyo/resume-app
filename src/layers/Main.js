import React, { Component } from 'react'
import Header from './Header'
import Profile from './Profile';
import { BrowserRouter, Route, Link } from "react-router-dom";
import Home from '../components/Home';
import Contact from '../components/Contact';
import Disclamer from '../components/Disclamer';
import Blog from '../components/Blog';
import Footer from './Footer';

/**
 * Contains the main content whose states we want to change and observe.
 * It has routing logics which renders components based on a spefic "path".
 * The main content is embbed between the "Header" and the Footer components.
 */
export class MainBody extends Component {
  constructor(properties) {
    super(properties)

    /*
     * The main urls used in routing.
     * Use this to add a Route "path" or "url" and access it through
     * <code>
     * this#paths#ROUTE_URL
     * <code/>
     * This avoid "magic values" when we explicitly hard-code variables
     * and makes our code more cleaner and readable.
     */
    this.paths = {
      HOME: "/",
      BLOG: "/blog",
      DISCLAIMER: "/disclaimer",
      CONTACT: "/contact",
    }
  }
  render() {
    return (
      <div className="main-content">
        <div>
           <Header />
        </div>
        
        <div class="container">
          <div class="row">
            <div class="col-sm">
              <Profile />
            </div>
            <div class="col-sm">
              <BrowserRouter>
                <div>
                  <Route exact path={this.paths.HOME} component={Home} />
                  <Route path={this.paths.CONTACT} component={Contact} />
                  <Route path={this.paths.BLOG} component={Blog} />
                  <Route path={this.paths.DISCLAIMER} component={Disclamer} />
                </div>
              </BrowserRouter>
            </div>
            </div>
          </div>
          <div>
            <Footer/>
          </div>
        </div>
        )
      }
    }
    
    export default MainBody
