
import React, { Component } from 'react'
import ImageProfile from "../assets/img/uncle_bob.jpg";
import { result } from '../assets/dataSet';
import { strings } from '../values/strings';

//TODO: Access this from a repository
const profile = result.personalDetail

const LinkDrawable = (props) => {
  return (
    <div>
      <i className={props.class}></i>
      <span>{props.label}</span>
    </div>
  )
}

const LinkExportPDF = () => {
  return (
    <div>
      <i class="fas fa-save"></i>
      <span>{strings.action_export_as_pdf}</span>
    </div>
  )
}

export class ProfileComponent extends Component {
  constructor(properties) {
    super(properties)
  }
  render() {
    const renderContactScreen = () => {
      //TODO: implement this with flux
      console.log("rendring conctact")
    }
    return (
      <div className="card profile">
        <div>
          <img src={ImageProfile} alt="Avatar" />
          <p>{profile.name}</p>
          <p>{profile.profession}</p>
          <div>
            <div role={strings.accesibility_button} onClick={this.renderContactScreen}>
              <i className="fas fa-link"></i>
              {strings.action_sendmessage}
            </div>
          </div>
          <div>
            <LinkDrawable class="fas fa-link" label={strings.personal_website_url} />
            <LinkDrawable class="fab fa-twitter" label={strings.action_twitter} />
            <LinkDrawable class="fab fa-linkedin-in" label={strings.action_linkedin} />
          </div>

          <LinkExportPDF />
        </div>

      </div>
    )
  }
}

export default ProfileComponent
