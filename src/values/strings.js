export const strings = {
    /*Titles and labels */
    personal_website_url: "cleancoder.com",
    /*Accessibility*/
    accesibility_button: "button",


    /*Action Texts */
    action_sendmessage: "Send message",
    action_linkedin: "linkedin",
    action_twitter: "twitter",
    action_export_as_pdf: "Export page as PDF"
}