
export const result = {
    personalDetail : {
        name:"Robert Martin",
        profileImage:"./assets/img/uncle_bob.jpg",
        profession:"Software Craftsman",
        links: {
            twitter:"https://twitter.com/@unclebobmartin",
            linkedin:"https://www.linkedin.com/in/robert-martin-7395b0/",
            website:"http://t.co/cEFTI5CDhO"
        }
    },

    background:{
        summary: "Robert C. Martin, colloquially known as \"Uncle Bob\", is an American software engineer and instructor. He is best known for being one of the authors of the Agile Manifesto and for developing several software design principles. He was also the editor-in-chief of C++ Report magazine and served as the first chairman of the Agile Alliance.",
        experience: [
             {
                company: "Uncle Bob Consulting LLC.",
                duration:"2010 - Present",
                position:"CEO"
             },
             {
                company: "Object Mentor.",
                duration:"1991 - 2010",
                postiion:"President"
             },

             {
                company: "Rational.",
                duration:"1990 - 19991",
                postiion:"Contractor"
             },
             {
                company: "Clear Communications, Inc",
                duration:"1986 - 1991",
                postiion:"Development Manager"
             },
             {
                company: "Teradyne Inc. - Telecommunications Division",
                duration:"1976 – 1988",
                postiion:"Development Manager"
             }
        ],
        education: [
            {
                institution: "Nitram Academy",
                programme:"Arch-Progelian Monadicator, Multi-transeint Meta-dynamic events",
                duration: "1952 - 2019"

            }
        ],

        skills: {
            language:[ "Java","C#","C++","Ruby","C"],
            knowledge: ["Scrum","TDD","Video Editing","Video Production"]
           
        },

        recommendations: [
            {
                name:"Jerry Fitzpatrick",
                profession:"Software Consultant",
                content:"I've known Bob for nearly thirty years, long before he was anyone's \"uncle\". We developed one of the first voice mail systems anywhere. Bob is a tireless learner, prolific writer, natural orator and true genius. He seems to have perpetual energy for all things software. Oh, and he's also a top-notch dungeon master!"
            },
            {
                name:"James Grenning",
                profession:"Coaching and training - Winggan Software",
                content:"I first met Bob in 1981 when I was interviewing for a job as a Software Engineer at Teradyne in Northbrook, Illinois. His passion for software sold me on joining Teradyne, where we developed microprocessor based electronic test equipment. I worked alongside Bob for 7 years at Teradyne. I joined Bob again at Object Mentor in the mid 90's. True to his company's name, Bob has been a great mentor to me and thousands of software engineers around the world. The principles he has compiled and defined have changed software development for so many.\nIf he is speaking at a conference, you must hear him speak. If you don't know what to do about the mess your code is in, have Bob help. His clear thinking, and insights are without equal. His ability to shake up the status quo and get engineers and managers to see better ways of developing makes working with Bob a great and productive experience. I am happy to give Bob my wholehearted recommendation."
            }
        ],

        posts: [
            {
            title: `<h2>The Clean Architecture"</h2>`,
            date : `<p>13 August 2012</p>`,
            content:`<p>Over the last several years we’ve seen a whole range of ideas regarding the architecture of systems. These include:

            Hexagonal Architecture (a.k.a. Ports and Adapters) by Alistair Cockburn and adopted by Steve Freeman, and Nat Pryce in their wonderful book Growing Object Oriented Software
            Onion Architecture by Jeffrey Palermo
            Screaming Architecture from a blog of mine last year
            DCI from James Coplien, and Trygve Reenskaug.
            BCE by Ivar Jacobson from his book Object Oriented Software Engineering: A Use-Case Driven Approach
            Though these architectures all vary somewhat in their details, they are very similar. They all have the same objective, which is the separation of concerns. They all achieve this separation by dividing the software into layers. Each has at least one layer for business rules, and another for interfaces.
            
            Each of these architectures produce systems that are:
            
            Independent of Frameworks. The architecture does not depend on the existence of some library of feature laden software. This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.
            Testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.
            Independent of UI. The UI can change easily, without changing the rest of the system. A Web UI could be replaced with a console UI, for example, without changing the business rules.
            Independent of Database. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else. Your business rules are not bound to the database.
            Independent of any external agency. In fact your business rules simply don’t know anything at all about the outside world.
            The diagram at the top of this article is an attempt at integrating all these architectures into a single actionable idea.
            
            The Dependency Rule
            The concentric circles represent different areas of software. In general, the further in you go, the higher level the software becomes. The outer circles are mechanisms. The inner circles are policies.
            
            The overriding rule that makes this architecture work is The Dependency Rule. This rule says that source code dependencies can only point inwards. Nothing in an inner circle can know anything at all about something in an outer circle. In particular, the name of something declared in an outer circle must not be mentioned by the code in the an inner circle. That includes, functions, classes. variables, or any other named software entity.
            
            By the same token, data formats used in an outer circle should not be used by an inner circle, especially if those formats are generate by a framework in an outer circle. We don’t want anything in an outer circle to impact the inner circles.
            
            Entities
            Entities encapsulate Enterprise wide business rules. An entity can be an object with methods, or it can be a set of data structures and functions. It doesn’t matter so long as the entities could be used by many different applications in the enterprise.
            
            If you don’t have an enterprise, and are just writing a single application, then these entities are the business objects of the application. They encapsulate the most general and high-level rules. They are the least likely to change when something external changes. For example, you would not expect these objects to be affected by a change to page navigation, or security. No operational change to any particular application should affect the entity layer.
            
            Use Cases
            The software in this layer contains application specific business rules. It encapsulates and implements all of the use cases of the system. These use cases orchestrate the flow of data to and from the entities, and direct those entities to use their enterprise wide business rules to achieve the goals of the use case.
            
            We do not expect changes in this layer to affect the entities. We also do not expect this layer to be affected by changes to externalities such as the database, the UI, or any of the common frameworks. This layer is isolated from such concerns.
            
            We do, however, expect that changes to the operation of the application will affect the use-cases and therefore the software in this layer. If the details of a use-case change, then some code in this layer will certainly be affected.
            
            Interface Adapters
            The software in this layer is a set of adapters that convert data from the format most convenient for the use cases and entities, to the format most convenient for some external agency such as the Database or the Web. It is this layer, for example, that will wholly contain the MVC architecture of a GUI. The Presenters, Views, and Controllers all belong in here. The models are likely just data structures that are passed from the controllers to the use cases, and then back from the use cases to the presenters and views.
            
            Similarly, data is converted, in this layer, from the form most convenient for entities and use cases, into the form most convenient for whatever persistence framework is being used. i.e. The Database. No code inward of this circle should know anything at all about the database. If the database is a SQL database, then all the SQL should be restricted to this layer, and in particular to the parts of this layer that have to do with the database.
            
            Also in this layer is any other adapter necessary to convert data from some external form, such as an external service, to the internal form used by the use cases and entities.
            
            Frameworks and Drivers.
            The outermost layer is generally composed of frameworks and tools such as the Database, the Web Framework, etc. Generally you don’t write much code in this layer other than glue code that communicates to the next circle inwards.
            
            This layer is where all the details go. The Web is a detail. The database is a detail. We keep these things on the outside where they can do little harm.
            
            Only Four Circles?
            No, the circles are schematic. You may find that you need more than just these four. There’s no rule that says you must always have just these four. However, The Dependency Rule always applies. Source code dependencies always point inwards. As you move inwards the level of abstraction increases. The outermost circle is low level concrete detail. As you move inwards the software grows more abstract, and encapsulates higher level policies. The inner most circle is the most general.
            
            Crossing boundaries.
            At the lower right of the diagram is an example of how we cross the circle boundaries. It shows the Controllers and Presenters communicating with the Use Cases in the next layer. Note the flow of control. It begins in the controller, moves through the use case, and then winds up executing in the presenter. Note also the source code dependencies. Each one of them points inwards towards the use cases.
            
            We usually resolve this apparent contradiction by using the Dependency Inversion Principle. In a language like Java, for example, we would arrange interfaces and inheritance relationships such that the source code dependencies oppose the flow of control at just the right points across the boundary.
            
            For example, consider that the use case needs to call the presenter. However, this call must not be direct because that would violate The Dependency Rule: No name in an outer circle can be mentioned by an inner circle. So we have the use case call an interface (Shown here as Use Case Output Port) in the inner circle, and have the presenter in the outer circle implement it.
            
            The same technique is used to cross all the boundaries in the architectures. We take advantage of dynamic polymorphism to create source code dependencies that oppose the flow of control so that we can conform to The Dependency Rule no matter what direction the flow of control is going in.
            
            What data crosses the boundaries.
            Typically the data that crosses the boundaries is simple data structures. You can use basic structs or simple Data Transfer objects if you like. Or the data can simply be arguments in function calls. Or you can pack it into a hashmap, or construct it into an object. The important thing is that isolated, simple, data structures are passed across the boundaries. We don’t want to cheat and pass Entities or Database rows. We don’t want the data structures to have any kind of dependency that violates The Dependency Rule.
            
            For example, many database frameworks return a convenient data format in response to a query. We might call this a RowStructure. We don’t want to pass that row structure inwards across a boundary. That would violate The Dependency Rule because it would force an inner circle to know something about an outer circle.
            
            So when we pass data across a boundary, it is always in the form that is most convenient for the inner circle.
            
            Conclusion
            Conforming to these simple rules is not hard, and will save you a lot of headaches going forward. By separating the software into layers, and conforming to The Dependency Rule, you will create a system that is intrinsically testable, with all the benefits that implies. When any of the external parts of the system become obsolete, like the database, or the web framework, you can replace those obsolete elements with a minimum of fuss."`
            },
            {title: "The Dark Path",
            date : "11 January 2017",
            content:``}

            ,

            {title: "The Dark Path",
            date : "11 January 2017",
            content:`Over the last few months I’ve dabbled in two new languages. Swift and Kotlin. These two languages have a number of similarities. Indeed, the similarities are so stark that I wonder if this isn’t a new trend in our language churn. If so, it is a dark path.

            Both languages have integrated some functional characteristics. For example, they both have lambdas. This is a good thing, in general. The more we learn about functional programming, the better. These languages are both a far cry from a truly functional programming language; but every step in that direction is a good step.
            
            My problem is that both languages have doubled down on strong static typing. Both seem to be intent on closing every single type hole in their parent languages. In the case of Swift, the parent language is the bizarre typeless hybrid of C and Smalltalk called Objective-C; so perhaps the emphasis on typing is understandable. In the case of Kotlin the parent is the already rather strongly typed Java.
            
            Now I don’t want you to think that I’m opposed to statically typed languages. I’m not. There are definite advantages to both dynamic and static languages; and I happily use both kinds. I have a slight preference for dynamic typing; and so I use Clojure quite a bit. On the other hand, I probably write more Java than Clojure. So you can consider me bi-typical. I walk on both sides of the street – so to speak.
            
            It’s not the fact that Swift and Kotlin are statically typed that has me concerned. Rather, it is the depth of that static typing.
            
            I would not call Java a strongly opinionated language when it comes to static typing. You can create structures in Java that follow the type rules nicely; but you can also violate many of the type rules whenever you want or need to. The language complains a bit when you do; and throws up a few roadblocks; but not so many as to be obstructionist.
            
            Swift and Kotlin, on the other hand, are completely inflexible when it comes to their type rules. For example, in Swift, if you declare a function to throw an exception, then by God every call to that function, all the way up the stack, must be adorned with a do-try block, or a try!, or a try?. There is no way, in this language, to silently throw an exception all the way to the top level; without paving a super-hiway for it up through the entire calling tree. (You can watch Justin and I struggle with this in our Mobile Application Case Study videos.)
            
            Now, perhaps you think this is a good thing. Perhaps you think that there have been a lot of bugs in systems that have resulted from un-corralled exceptions. Perhaps you think that exceptions that aren’t escorted, step by step, up the calling stack are risky and error prone. And, of course, you would be right about that. Undeclared and unmanaged exceptions are very risky.
            
            The question is: Whose job is it to manage that risk? Is it the language’s job? Or is it the programmer’s job?
            
            In Kotlin, you cannot derive from a class, or override a function, unless you adorn that class or function as open. You also cannot override a function unless the overriding function is adorned with override. If you neglect to adorn a class with open, the language will not allow you to derive from it.
            
            Now, perhaps you think this is a good thing. Perhaps you believe that inheritance and derivation hierarchies that are allowed to grow without bound are a source of error and risk. Perhaps you think we can eliminate whole classes of bugs by forcing programmers to explicitly declare their classes to be open. And you may be right. Derivation and inheritance are risky things. Lots can go wrong when you override a function in a derived class.
            
            The question is: Whose job is it to manage that risk? Is it the language’s job? Or is it the programmer’s job.
            
            Both Swift and Kotlin have incorporated the concept of nullable types. The fact that a variable can contain a null becomes part of the type of that variable. A variable of type String cannot contain a null; it can only contain a reified String. On the other hand, a variable of type String? has a nullable type and can contain a null.
            
            The rules of the language insist that when you use a nullable variable, you must first check that variable for null. So if s is a String? then var l = s.length() won’t compile. Instead you have to say var l = s.length() ?: 0 or var l = if (s!=null) s.length() else 0.
            
            Perhaps you think this is a good thing. Perhaps you have seen enough NPEs in your lifetime. Perhaps you know, beyond a shadow of a doubt, that unchecked nulls are the cause of billions and billions of dollars of software failures. (Indeed, the Kotlin documentation calls the NPE the “Billion Dollar Bug”). And, of course, you are right. It is very risky to have nulls rampaging around the system out of control.
            
            The question is: Whose job is it to manage the nulls. The language? Or the programmer?
            
            These languages are like the little Dutch boy sticking his fingers in the dike. Every time there’s a new kind of bug, we add a language feature to prevent that kind of bug. And so these languages accumulate more and more fingers in holes in dikes. The problem is, eventually you run out of fingers and toes.
            
            But before you run out of fingers and toes, you have created languages that contain dozens of keywords, hundreds of constraints, a tortuous syntax, and a reference manual that reads like a law book. Indeed, to become an expert in these languages, you must become a language lawyer (a term that was invented during the C++ era.)
            
            This is the wrong path!
            
            Ask yourself why we are trying to plug defects with language features. The answer ought to be obvious. We are trying to plug these defects because these defects happen too often.
            
            Now, ask yourself why these defects happen too often. If your answer is that our languages don’t prevent them, then I strongly suggest that you quit your job and never think about being a programmer again; because defects are never the fault of our languages. Defects are the fault of programmers. It is programmers who create defects – not languages.
            
            And what is it that programmers are supposed to do to prevent defects? I’ll give you one guess. Here are some hints. It’s a verb. It starts with a “T”. Yeah. You got it. TEST!
            
            You test that your system does not emit unexpected nulls. You test that your system handles nulls at it’s inputs. You test that every exception you can throw is caught somewhere.
            
            Why are these languages adopting all these features? Because programmers are not testing their code. And because programmers are not testing their code, we now have languages that force us to put the word open in front of every class we want to derive from. We now have languages that force us to adorn every function, all the way up the calling tree, with try!. We now have languages that are so constraining, and so over-specified, that you have to design the whole system up front before you can code any of it.
            
            Consider: How do I know whether a class is open or not? How do I know if somewhere down the calling tree someone might throw an exception? How much code will I have to change when I finally discover that someone really needs to return a null up the calling tree?
            
            All these constraints, that these languages are imposing, presume that the programmer has perfect knowledge of the system; before the system is written. They presume that you know which classes will need to be open and which will not. They presume that you know which calling paths will throw exceptions, and which will not. They presume that you know which functions will produce null and which will not.
            
            And because of all this presumption, they punish you when you are wrong. They force you to go back and change massive amounts of code, adding try! or ?: or open all the way up the stack.
            
            And how do you avoid being punished? There are two ways. One that works; and one that doesn’t. The one that doesn’t work is to design everything up front before coding. The one that does avoid the punishment is to override all the safeties.
            
            And so you will declare all your classes and all your functions open. You will never use exceptions. And you will get used to using lots and lots of ! characters to override the null checks and allow NPEs to rampage through your systems.
            
            Why did the nuclear plant at Chernobyl catch fire, melt down, destroy a small city, and leave a large area uninhabitable? They overrode all the safeties. So don’t depend on safeties to prevent catastrophes. Instead, you’d better get used to writing lots and lots of tests, no matter what language you are using!`}

        ]
    }   
}